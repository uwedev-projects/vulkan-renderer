#pragma once

#include <cstdint>
#include <optional>
#include <utility>

namespace Uwe
{
	using u8 = std::uint8_t;
	using u16 = std::uint16_t;
	using u32 = std::uint32_t;
	using u64 = std::uint64_t;

	using s8 = std::int8_t;
	using s16 = std::int16_t;
	using s32 = std::int32_t;
	using s64 = std::int64_t;

	using f32 = float;
	using f64 = double;

	enum class Err : s32 {
		OK = 0,
		Warn,
		Failed
	};

	template <typename T>
	struct Result {
		std::optional<T> value;
		Err error;

		template <typename... Ts>
		Result(Err error, Ts&&... args)
			: value{ std::optional<T>{std::forward<decltype(args)>(args)...} }, error{ error }
		{

		}
	};

#ifndef _NDEBUG
	constexpr bool IsDebug = true;
#else
	constexpr bool IsDebug = false;
#endif
}