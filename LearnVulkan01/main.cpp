﻿#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtx/hash.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stb_image.h>
#include <tiny_obj_loader.h>

#include "Core.h"
#include "FileSystem.h"

#include <unordered_map>
#include <iostream>
#include <algorithm>
#include <optional>
#include <memory>
#include <vector>
#include <string>
#include <limits>
#include <chrono>
#include <array>
#include <set>

#include <cstring>

using namespace Uwe;

namespace App {

    constexpr s32 WindowWidth = 800;
    constexpr s32 WindowHeight = 600;

    constexpr const char* ModelPath = "data/models/viking_room.obj";
    constexpr const char* TexturePath = "data/textures/viking_room.png";

    constexpr std::array<const char*, 1> ValidationLayers{
         "VK_LAYER_KHRONOS_validation"
    };

    constexpr std::array<const char*, 1> DeviceExtensions{
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };

    struct SwapChainCapabilities {
        VkSurfaceCapabilitiesKHR surface;
        std::vector<VkSurfaceFormatKHR> formats;
        std::vector<VkPresentModeKHR> presentModes;
    };

    struct Vertex {
        glm::vec3 position;
        glm::vec3 color;
        glm::vec2 uv;

        friend bool operator==(const Vertex& rhs, const Vertex& lhs);
    };

    bool operator==(const Vertex& rhs, const Vertex& lhs) {
        return rhs.position == lhs.position && rhs.uv == lhs.uv && rhs.color == lhs.color;
    }

}

namespace std {
    template<>
    struct hash<App::Vertex> {
        size_t operator()(const App::Vertex& vertex) const {
            return ((hash<glm::vec3>()(vertex.position) ^
                (hash<glm::vec2>()(vertex.uv) << 1)) >> 1) ^
                (hash<glm::vec3>()(vertex.color) << 1);
        }
    };
}

namespace App {

    struct UniformBufferObject {
        glm::mat4 alignas(16) model;
        glm::mat4 alignas(16) view;
        glm::mat4 alignas(16) proj;
    };

    VkVertexInputBindingDescription getBindingDescription() {
        VkVertexInputBindingDescription bindingDescription{};

        bindingDescription.binding = 0;
        bindingDescription.stride = sizeof(Vertex);
        bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

        return bindingDescription;
    }

    std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions() {
        std::vector<VkVertexInputAttributeDescription> attributeDescriptions{};
        attributeDescriptions.resize(3);

        attributeDescriptions[0].binding = 0;
        attributeDescriptions[0].location = 0;
        attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
        attributeDescriptions[0].offset = offsetof(Vertex, position);

        attributeDescriptions[1].binding = 0;
        attributeDescriptions[1].location = 1;
        attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
        attributeDescriptions[1].offset = offsetof(Vertex, color);

        attributeDescriptions[2].binding = 0;
        attributeDescriptions[2].location = 2;
        attributeDescriptions[2].format = VK_FORMAT_R32G32B32_SFLOAT;
        attributeDescriptions[2].offset = offsetof(Vertex, uv);

        return attributeDescriptions;
    }

    bool checkValidationLayerSupport() {
        u32 layerCount = 0;
        vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

        std::vector<VkLayerProperties> availableLayers(layerCount);
        vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

        bool layerSupport = true;
        for (const auto layer : ValidationLayers) {
            const auto itAvailable = std::find_if(availableLayers.begin(), availableLayers.end(), [layer](const VkLayerProperties& layerProps) {
                return std::strcmp(layer, layerProps.layerName) == 0;
            });

            if (itAvailable == availableLayers.end()) {
                layerSupport = false;
                break;
            }

        }

        return layerSupport;
    }

    VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) {
        for (const auto& availableFormat : availableFormats) {
            if (availableFormat.format == VK_FORMAT_B8G8R8A8_SRGB && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
                return availableFormat;
            }
        }

        return availableFormats[0];
    }

    VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes) {
        for (const auto& availablePresentMode : availablePresentModes) {
            if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
                return availablePresentMode;
            }
        }

        return VK_PRESENT_MODE_FIFO_KHR;
    }

    VkExtent2D chooseSwapExtent(GLFWwindow* window, const VkSurfaceCapabilitiesKHR& capabilities) {
        if (capabilities.currentExtent.width != std::numeric_limits<u32>::max()) {
            return capabilities.currentExtent;
        }
        else {
            s32 width = 0, height = 0;
            glfwGetFramebufferSize(window, &width, &height);

            VkExtent2D actualExtent = {
                static_cast<u32>(width),
                static_cast<u32>(height)
            };

            actualExtent.width = std::clamp(actualExtent.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
            actualExtent.height = std::clamp(actualExtent.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);

            return actualExtent;
        }
    }

    void framebufferResizeCallback(GLFWwindow* window, int width, int height);

    class HelloTri {
    public:
        Err run() {
            if (initWindow() != Err::OK)
                return Err::Failed;

            if (initVulkan() != Err::OK)
                return Err::Failed;

            if (mainLoop() != Err::OK)
                return Err::Failed;

            cleanup();
        }

        HelloTri() :
            window{},
            vulkanInstance{ VK_NULL_HANDLE },
            device{ VK_NULL_HANDLE },
            physicalDevice{ VK_NULL_HANDLE },
            graphicsQueue{ VK_NULL_HANDLE },
            presentQueue{ VK_NULL_HANDLE },
            surface{ VK_NULL_HANDLE },
            swapChain{ VK_NULL_HANDLE },
            swapChainImages{},
            swapChainImageViews{},
            framebuffers{},
            swapChainImageFormat{},
            swapChainExtent{},
            renderPass{ VK_NULL_HANDLE },
            pipelineLayout{ VK_NULL_HANDLE },
            pipeline{ VK_NULL_HANDLE },
            msaaSamples{ VK_SAMPLE_COUNT_1_BIT },
            graphicsFamily{},
            presentFamily{},
            currentFrame{},
            resized{}
        {

        }

        inline void frameBufferResized(bool isResized) {
            resized = isResized;
        }

    private:

        using Clock = std::chrono::high_resolution_clock;
        using Seconds = std::chrono::duration<f32, std::chrono::seconds::period>;

        static constexpr std::size_t NumConcurrentFrames = 2;
        using CommandBuffers = std::array<VkCommandBuffer, NumConcurrentFrames>;
        using Semaphores = std::array<VkSemaphore, NumConcurrentFrames>;
        using Fences = std::array<VkFence, NumConcurrentFrames>;
        using UniformBuffers = std::array<VkBuffer, NumConcurrentFrames>;
        using UniformBuffersMemory = std::array<VkDeviceMemory, NumConcurrentFrames>;
        using MappedUniformBuffers = std::array<void*, NumConcurrentFrames>;

        GLFWwindow* window;
        VkInstance vulkanInstance;
        VkDevice device;
        VkPhysicalDevice physicalDevice;
        VkQueue graphicsQueue;
        VkQueue presentQueue;
        VkSurfaceKHR surface;
        VkSwapchainKHR swapChain;
        std::vector<VkImage> swapChainImages;
        std::vector<VkImageView> swapChainImageViews;
        std::vector<VkFramebuffer> framebuffers;
        VkFormat swapChainImageFormat;
        VkExtent2D swapChainExtent;
        VkRenderPass renderPass;
        VkDescriptorPool descriptorPool{};
        VkDescriptorSetLayout descriptorSetLayout;
        std::vector<VkDescriptorSet> descriptorSets;
        VkPipelineLayout pipelineLayout;
        VkPipeline pipeline;
        VkCommandPool commandPool;
        CommandBuffers commandBuffers;
        Semaphores imageReadySemaphores;
        Semaphores doneRenderingSemaphores;
        Fences inFlightFences;
        UniformBuffers uniformBuffers;
        UniformBuffersMemory uniformBuffersMemory;
        MappedUniformBuffers mappedUniformBuffers;
        std::vector<Vertex> verts;
        std::vector<u32> indices;
        VkBuffer vertexBuffer;
        VkDeviceMemory vertexBufferMemory;
        VkBuffer indexBuffer;
        VkDeviceMemory indexBufferMemory;
        VkImage colorImage;
        VkImageView colorImageView;
        VkDeviceMemory colorImageMemory;
        VkImage textureImage;
        VkImageView textureImageView;
        VkSampler textureSampler;
        VkDeviceMemory textureImageMemory;
        VkImage depthImage;
        VkDeviceMemory depthImageMemory;
        VkImageView depthImageView;
        VkSampleCountFlagBits msaaSamples;
        std::optional<u32> graphicsFamily;
        std::optional<u32> presentFamily;
        u32 currentFrame;
        u32 mipLevels;
        bool resized;

        Err createInstance() {
            VkApplicationInfo appInfo{};
            appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
            appInfo.pApplicationName = "Hello Triangle";
            appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
            appInfo.pEngineName = "No Engine";
            appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
            appInfo.apiVersion = VK_API_VERSION_1_0;

            VkInstanceCreateInfo createInfo{};
            createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
            createInfo.pApplicationInfo = &appInfo;

            u32 glfwExtensionCount = 0;
            const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
            createInfo.enabledExtensionCount = glfwExtensionCount;
            createInfo.ppEnabledExtensionNames = glfwExtensions;

            u32 extensionCount = 0;
            vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);

            std::vector<VkExtensionProperties> extensions(extensionCount);
            vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data());

            if constexpr (IsDebug) {
                if (!checkValidationLayerSupport()) {
                    return Err::Failed;
                }

                createInfo.enabledLayerCount = static_cast<u32>(ValidationLayers.size());
                createInfo.ppEnabledLayerNames = ValidationLayers.data();
            }
            else {
                createInfo.enabledLayerCount = 0;
            }

            const VkResult result = vkCreateInstance(&createInfo, nullptr, &vulkanInstance);
            return result == VK_SUCCESS ? Err::OK : Err::Failed;
        }

        bool checkDeviceExtensionSupport(VkPhysicalDevice deviceToTest) const {
            u32 extensionCount = 0;
            vkEnumerateDeviceExtensionProperties(deviceToTest, nullptr, &extensionCount, nullptr);

            std::vector<VkExtensionProperties> availableExtensions(extensionCount);
            vkEnumerateDeviceExtensionProperties(deviceToTest, nullptr, &extensionCount, availableExtensions.data());

            std::set<std::string> requiredExtensions(DeviceExtensions.begin(), DeviceExtensions.end());

            for (const auto& extension : availableExtensions) {
                requiredExtensions.erase(extension.extensionName);
            }

            return requiredExtensions.empty();
        }

        SwapChainCapabilities querySwapChainCapabilities(VkPhysicalDevice deviceToTest) {
            SwapChainCapabilities capabilities{};

            vkGetPhysicalDeviceSurfaceCapabilitiesKHR(deviceToTest, surface, &capabilities.surface);

            u32 formatCount = 0;
            vkGetPhysicalDeviceSurfaceFormatsKHR(deviceToTest, surface, &formatCount, nullptr);

            if (formatCount != 0) {
                capabilities.formats.resize(formatCount);
                vkGetPhysicalDeviceSurfaceFormatsKHR(deviceToTest, surface, &formatCount, capabilities.formats.data());
            }

            u32 presentModeCount = 0;
            vkGetPhysicalDeviceSurfacePresentModesKHR(deviceToTest, surface, &presentModeCount, nullptr);

            if (presentModeCount != 0) {
                capabilities.presentModes.resize(presentModeCount);
                vkGetPhysicalDeviceSurfacePresentModesKHR(deviceToTest, surface, &presentModeCount, capabilities.presentModes.data());
            }

            return capabilities;
        }

        Err pickPhysicalDevice() {
            u32 deviceCount = 0;
            vkEnumeratePhysicalDevices(vulkanInstance, &deviceCount, nullptr);

            if (deviceCount == 0)
                return Err::Failed;

            std::vector<VkPhysicalDevice> devices(deviceCount);
            vkEnumeratePhysicalDevices(vulkanInstance, &deviceCount, devices.data());

            const auto canUseDevice = [this](const VkPhysicalDevice deviceToTest) {
                VkPhysicalDeviceProperties deviceProperties{};
                VkPhysicalDeviceFeatures deviceFeatures{};
                vkGetPhysicalDeviceProperties(deviceToTest, &deviceProperties);
                vkGetPhysicalDeviceFeatures(deviceToTest, &deviceFeatures);

                u32 queueFamilyCount = 0;
                vkGetPhysicalDeviceQueueFamilyProperties(deviceToTest, &queueFamilyCount, nullptr);

                std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
                vkGetPhysicalDeviceQueueFamilyProperties(deviceToTest, &queueFamilyCount, queueFamilies.data());

                for (int i = 0; i < queueFamilyCount; ++i) {
                    VkBool32 presentSupport = false;
                    vkGetPhysicalDeviceSurfaceSupportKHR(deviceToTest, i, surface, &presentSupport);
                    if (!presentFamily.has_value() && presentSupport) {
                        presentFamily = i;
                    }

                    if (!graphicsFamily.has_value() && (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)) {
                        graphicsFamily = i;
                    }
                }

                const SwapChainCapabilities capabilities{ querySwapChainCapabilities(deviceToTest) };

                return deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU &&
                    deviceFeatures.geometryShader && graphicsFamily.has_value() && presentFamily.has_value() &&
                    checkDeviceExtensionSupport(deviceToTest) && !capabilities.formats.empty() && !capabilities.presentModes.empty() &&
                    deviceFeatures.samplerAnisotropy;
            };

            const auto itDevice = std::find_if(devices.begin(), devices.end(), canUseDevice);
            if (itDevice == devices.end())
                return Err::Failed;

            physicalDevice = *itDevice;
            msaaSamples = getMaxUsableSampleCount();
            return Err::OK;
        }

        Err createLogicalDevice() {
            std::vector<VkDeviceQueueCreateInfo> queueCreateInfos{};
            const std::set<u32> queues{ graphicsFamily.value(), presentFamily.value() };

            f32 queuePriority = 1.0f;

            for (const u32 queue : queues) {
                VkDeviceQueueCreateInfo queueCreateInfo{};
                queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
                queueCreateInfo.queueFamilyIndex = graphicsFamily.value();
                queueCreateInfo.queueCount = 1;
                queueCreateInfo.pQueuePriorities = &queuePriority;
                queueCreateInfos.push_back(queueCreateInfo);
            }

            VkPhysicalDeviceFeatures deviceFeatures{};
            deviceFeatures.samplerAnisotropy = VK_TRUE;

            VkDeviceCreateInfo createInfo{};
            createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
            createInfo.pQueueCreateInfos = queueCreateInfos.data();
            createInfo.queueCreateInfoCount = static_cast<u32>(queueCreateInfos.size());
            createInfo.pEnabledFeatures = &deviceFeatures;

            createInfo.enabledExtensionCount = static_cast<u32>(DeviceExtensions.size());
            createInfo.ppEnabledExtensionNames = DeviceExtensions.data();

            if constexpr (IsDebug) {
                createInfo.enabledLayerCount = static_cast<u32>(ValidationLayers.size());
                createInfo.ppEnabledLayerNames = ValidationLayers.data();
            }
            else {
                createInfo.enabledLayerCount = 0;
            }

            if (vkCreateDevice(physicalDevice, &createInfo, nullptr, &device) != VK_SUCCESS)
                return Err::Failed;

            vkGetDeviceQueue(device, graphicsFamily.value(), 0, &graphicsQueue);
            vkGetDeviceQueue(device, presentFamily.value(), 0, &presentQueue);

            return Err::OK;
        }

        Err createSurface() {
            if (glfwCreateWindowSurface(vulkanInstance, window, nullptr, &surface) != VK_SUCCESS)
                return Err::Failed;

            return Err::OK;
        }

        Err createSwapChain() {
            SwapChainCapabilities capabilities = querySwapChainCapabilities(physicalDevice);

            const VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(capabilities.formats);
            const VkPresentModeKHR presentMode = chooseSwapPresentMode(capabilities.presentModes);
            swapChainExtent = chooseSwapExtent(window, capabilities.surface);
            swapChainImageFormat = surfaceFormat.format;

            const u32 imageCount = std::clamp(capabilities.surface.minImageCount + 1, 
                capabilities.surface.minImageCount + 1, capabilities.surface.maxImageCount);

            VkSwapchainCreateInfoKHR createInfo{};
            createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
            createInfo.surface = surface;

            createInfo.minImageCount = imageCount;
            createInfo.imageFormat = surfaceFormat.format;
            createInfo.imageColorSpace = surfaceFormat.colorSpace;
            createInfo.imageExtent = swapChainExtent;
            createInfo.imageArrayLayers = 1;
            createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

            const std::vector<u32> familyIndices{ graphicsFamily.value(), presentFamily.value() };
            if (graphicsFamily.value() != presentFamily.value()) {
                createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
                createInfo.queueFamilyIndexCount = static_cast<u32>(familyIndices.size());
                createInfo.pQueueFamilyIndices = familyIndices.data();
            }
            else {
                createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
                createInfo.queueFamilyIndexCount = 0; // Optional
                createInfo.pQueueFamilyIndices = nullptr; // Optional
            }

            createInfo.preTransform = capabilities.surface.currentTransform;
            createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

            createInfo.presentMode = presentMode;
            createInfo.clipped = VK_TRUE;
            createInfo.oldSwapchain = VK_NULL_HANDLE;

            if (vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapChain) != VK_SUCCESS)
                return Err::Failed;

            u32 swapChainImageCount = 0;
            vkGetSwapchainImagesKHR(device, swapChain, &swapChainImageCount, nullptr);
            swapChainImages.resize(swapChainImageCount);
            vkGetSwapchainImagesKHR(device, swapChain, &swapChainImageCount, swapChainImages.data());

            return Err::OK;
        }

        Err recreateSwapChain() {
            int width = 0, height = 0;
            glfwGetFramebufferSize(window, &width, &height);
            while (width == 0 || height == 0) {
                glfwGetFramebufferSize(window, &width, &height);
                glfwWaitEvents();
            }

            vkDeviceWaitIdle(device);
            cleanupSwapChain();

            if (createSwapChain() != Err::OK)
                return Err::Failed;

            if (createImageViews() != Err::OK)
                return Err::Failed;

            if (createColorResources() != Err::OK)
                return Err::Failed;

            if (createDepthResources() != Err::OK)
                return Err::Failed;

            if (createFramebuffers() != Err::OK)
                return Err::Failed;

            return Err::OK;
        }

        Err createImageViews() {
            Err err = Err::OK;
            swapChainImageViews.resize(swapChainImages.size());
            for (int i = 0; i < swapChainImages.size() && err == Err::OK; ++i) {
                err = createImageView(swapChainImages[i], swapChainImageFormat, VK_IMAGE_ASPECT_COLOR_BIT, swapChainImageViews[i], 1);
            }

            return err;
        }

        Err createTextureSampler() {
            VkSamplerCreateInfo samplerInfo{};
            samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
            samplerInfo.magFilter = VK_FILTER_LINEAR;
            samplerInfo.minFilter = VK_FILTER_LINEAR;

            samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
            samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
            samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;

            VkPhysicalDeviceProperties properties{};
            vkGetPhysicalDeviceProperties(physicalDevice, &properties);

            samplerInfo.anisotropyEnable = VK_TRUE;
            samplerInfo.maxAnisotropy = properties.limits.maxSamplerAnisotropy;
            samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
            samplerInfo.unnormalizedCoordinates = VK_FALSE;

            samplerInfo.compareEnable = VK_FALSE;
            samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
            samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
            samplerInfo.minLod = 0.0f;
            samplerInfo.maxLod = static_cast<f32>(mipLevels);
            samplerInfo.mipLodBias = 0.0f; // Optional

            if (vkCreateSampler(device, &samplerInfo, nullptr, &textureSampler) != VK_SUCCESS)
                return Err::Failed;

            return Err::OK;
        }

        std::optional<VkShaderModule> createShaderModule(const std::vector<char>& byteCode) {
            VkShaderModuleCreateInfo createInfo{};
            createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
            createInfo.codeSize = byteCode.size();
            createInfo.pCode = reinterpret_cast<const uint32_t*>(byteCode.data());

            VkShaderModule shaderModule{};
            if (vkCreateShaderModule(device, &createInfo, nullptr, &shaderModule) != VK_SUCCESS)
                return std::nullopt;

            return shaderModule;
        }

        Err createDescriptorSetLayout() {
            VkDescriptorSetLayoutBinding uboLayoutBinding{};
            uboLayoutBinding.binding = 0;
            uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            uboLayoutBinding.descriptorCount = 1;

            uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
            uboLayoutBinding.pImmutableSamplers = nullptr; // Optional

            VkDescriptorSetLayoutBinding samplerLayoutBinding{};
            samplerLayoutBinding.binding = 1;
            samplerLayoutBinding.descriptorCount = 1;
            samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            samplerLayoutBinding.pImmutableSamplers = nullptr;
            samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

            std::array<VkDescriptorSetLayoutBinding, 2> layoutBindings{ uboLayoutBinding, samplerLayoutBinding };

            VkDescriptorSetLayoutCreateInfo layoutInfo{};
            layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
            layoutInfo.bindingCount = static_cast<u32>(layoutBindings.size());
            layoutInfo.pBindings = layoutBindings.data();

            if (vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &descriptorSetLayout) != VK_SUCCESS)
                return Err::Failed;

            return Err::OK;
        }

        Err createPipeline() {
            Fs::FileHandle vertexShaderFile{ Fs::Path{"shaders\\HelloTri.vert.spv"}, "rb" };
            const std::vector<char> vertexShaderByteCode{ vertexShaderFile.read() };

            Fs::FileHandle fragShaderFile{ Fs::Path{"shaders\\HelloTri.frag.spv"}, "rb" };
            const std::vector<char> fragShaderByteCode{ fragShaderFile.read() };

            std::optional<VkShaderModule> vertexModule = createShaderModule(vertexShaderByteCode);
            std::optional<VkShaderModule> fragModule = createShaderModule(fragShaderByteCode);

            if (!vertexModule.has_value() || !fragModule.has_value())
                return Err::Failed;

            VkPipelineShaderStageCreateInfo vertShaderStageInfo{};
            vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;

            vertShaderStageInfo.module = *vertexModule;
            vertShaderStageInfo.pName = "main";

            VkPipelineShaderStageCreateInfo fragShaderStageInfo{};
            fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
            fragShaderStageInfo.module = *fragModule;
            fragShaderStageInfo.pName = "main";

            VkPipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };

            std::vector<VkDynamicState> dynamicStates = {
                VK_DYNAMIC_STATE_VIEWPORT,
                VK_DYNAMIC_STATE_SCISSOR
            };

            VkPipelineDynamicStateCreateInfo dynamicState{};
            dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
            dynamicState.dynamicStateCount = static_cast<u32>(dynamicStates.size());
            dynamicState.pDynamicStates = dynamicStates.data();

            VkPipelineViewportStateCreateInfo viewportState{};
            viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
            viewportState.viewportCount = 1;
            viewportState.scissorCount = 1;

            const VkVertexInputBindingDescription bindingDesc = getBindingDescription();
            const std::vector<VkVertexInputAttributeDescription> attrDescriptions = getAttributeDescriptions();

            VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
            vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
            vertexInputInfo.vertexBindingDescriptionCount = 1;
            vertexInputInfo.pVertexBindingDescriptions = &bindingDesc; // Optional
            vertexInputInfo.vertexAttributeDescriptionCount = static_cast<u32>(attrDescriptions.size());
            vertexInputInfo.pVertexAttributeDescriptions = attrDescriptions.data(); // Optional

            VkPipelineInputAssemblyStateCreateInfo inputAssembly{};
            inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
            inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
            inputAssembly.primitiveRestartEnable = VK_FALSE;

            VkPipelineRasterizationStateCreateInfo rasterizer{};
            rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
            rasterizer.depthClampEnable = VK_FALSE;
            rasterizer.rasterizerDiscardEnable = VK_FALSE;
            rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
            rasterizer.lineWidth = 1.0f;

            rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
            rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;

            rasterizer.depthBiasEnable = VK_FALSE;
            rasterizer.depthBiasConstantFactor = 0.0f; // Optional
            rasterizer.depthBiasClamp = 0.0f; // Optional
            rasterizer.depthBiasSlopeFactor = 0.0f; // Optional

            VkPipelineMultisampleStateCreateInfo multisampling{};
            multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
            multisampling.sampleShadingEnable = VK_FALSE;
            multisampling.rasterizationSamples = msaaSamples;
            multisampling.minSampleShading = 1.0f; // Optional
            multisampling.pSampleMask = nullptr; // Optional
            multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
            multisampling.alphaToOneEnable = VK_FALSE; // Optional

            VkPipelineColorBlendAttachmentState colorBlendAttachment{};
            colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
            colorBlendAttachment.blendEnable = VK_FALSE;
            colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
            colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
            colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD; // Optional
            colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
            colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
            colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD; // Optional

            VkPipelineColorBlendStateCreateInfo colorBlending{};
            colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
            colorBlending.logicOpEnable = VK_FALSE;
            colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
            colorBlending.attachmentCount = 1;
            colorBlending.pAttachments = &colorBlendAttachment;
            colorBlending.blendConstants[0] = 0.0f; // Optional
            colorBlending.blendConstants[1] = 0.0f; // Optional
            colorBlending.blendConstants[2] = 0.0f; // Optional
            colorBlending.blendConstants[3] = 0.0f; // Optional

            VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
            pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
            pipelineLayoutInfo.setLayoutCount = 1; // Optional
            pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout; // Optional
            pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
            pipelineLayoutInfo.pPushConstantRanges = nullptr; // Optional

            if (vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &pipelineLayout) != VK_SUCCESS)
                return Err::Failed;

            VkPipelineDepthStencilStateCreateInfo depthStencil{};
            depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
            depthStencil.depthTestEnable = VK_TRUE;
            depthStencil.depthWriteEnable = VK_TRUE;
            depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
            depthStencil.depthBoundsTestEnable = VK_FALSE;
            depthStencil.minDepthBounds = 0.0f; // Optional
            depthStencil.maxDepthBounds = 1.0f; // Optional
            depthStencil.stencilTestEnable = VK_FALSE;
            depthStencil.front = {}; // Optional
            depthStencil.back = {}; // Optional

            VkGraphicsPipelineCreateInfo pipelineInfo{};
            pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
            pipelineInfo.stageCount = 2;
            pipelineInfo.pStages = shaderStages;

            pipelineInfo.pVertexInputState = &vertexInputInfo;
            pipelineInfo.pInputAssemblyState = &inputAssembly;
            pipelineInfo.pViewportState = &viewportState;
            pipelineInfo.pRasterizationState = &rasterizer;
            pipelineInfo.pMultisampleState = &multisampling;
            pipelineInfo.pDepthStencilState = &depthStencil; // Optional
            pipelineInfo.pColorBlendState = &colorBlending;
            pipelineInfo.pDynamicState = &dynamicState;

            pipelineInfo.layout = pipelineLayout;
            pipelineInfo.renderPass = renderPass;
            pipelineInfo.subpass = 0;

            pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
            pipelineInfo.basePipelineIndex = -1; // Optional

            if (vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &pipeline) != VK_SUCCESS)
                return Err::Failed;

            vkDestroyShaderModule(device, *vertexModule, nullptr);
            vkDestroyShaderModule(device, *fragModule, nullptr);

            return Err::OK;
        }

        Err createRenderPass() {
            VkAttachmentDescription colorAttachment{};
            colorAttachment.format = swapChainImageFormat;
            colorAttachment.samples = msaaSamples;

            colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
            colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
            colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
            colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

            colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            colorAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

            VkAttachmentReference colorAttachmentRef{};
            colorAttachmentRef.attachment = 0;
            colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

            std::optional<VkFormat> depthFormat = findDepthFormat();
            if (!depthFormat.has_value())
                return Err::Failed;

            VkAttachmentDescription depthAttachment{};
            depthAttachment.format = depthFormat.value();
            depthAttachment.samples = msaaSamples;
            depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
            depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
            depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
            depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
            depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

            VkAttachmentReference depthAttachmentRef{};
            depthAttachmentRef.attachment = 1;
            depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

            VkAttachmentDescription colorResolveAttachment{};
            colorResolveAttachment.format = swapChainImageFormat;
            colorResolveAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
            colorResolveAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
            colorResolveAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
            colorResolveAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
            colorResolveAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
            colorResolveAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            colorResolveAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

            VkAttachmentReference colorResolveAttachmentRef{};
            colorResolveAttachmentRef.attachment = 2;
            colorResolveAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

            VkSubpassDescription subpass{};
            subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
            subpass.colorAttachmentCount = 1;
            subpass.pColorAttachments = &colorAttachmentRef;
            subpass.pDepthStencilAttachment = &depthAttachmentRef;
            subpass.pResolveAttachments = &colorResolveAttachmentRef;

            VkSubpassDependency dependency{};
            dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
            dependency.dstSubpass = 0;

            dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
            dependency.srcAccessMask = 0;

            dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
            dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

            std::array<VkAttachmentDescription, 3> attachments{ colorAttachment, depthAttachment, colorResolveAttachment };

            VkRenderPassCreateInfo renderPassInfo{};
            renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
            renderPassInfo.attachmentCount = static_cast<u32>(attachments.size());
            renderPassInfo.pAttachments = attachments.data();
            renderPassInfo.subpassCount = 1;
            renderPassInfo.pSubpasses = &subpass;
            renderPassInfo.dependencyCount = 1;
            renderPassInfo.pDependencies = &dependency;

            if (vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass) != VK_SUCCESS)
                return Err::Failed;

            return Err::OK;
        }

        Err createFramebuffers() {
            framebuffers.resize(swapChainImageViews.size());

            for (size_t i = 0; i < swapChainImageViews.size(); i++) {
                std::array<VkImageView, 3> attachments = {
                    colorImageView,
                    depthImageView,
                    swapChainImageViews[i]
                };

                VkFramebufferCreateInfo framebufferInfo{};
                framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
                framebufferInfo.renderPass = renderPass;
                framebufferInfo.attachmentCount = static_cast<u32>(attachments.size());
                framebufferInfo.pAttachments = attachments.data();
                framebufferInfo.width = swapChainExtent.width;
                framebufferInfo.height = swapChainExtent.height;
                framebufferInfo.layers = 1;

                if (vkCreateFramebuffer(device, &framebufferInfo, nullptr, &framebuffers[i]) != VK_SUCCESS)
                    return Err::Failed;
            }

            return Err::OK;
        }

        Err createCommandPool() {
            VkCommandPoolCreateInfo poolInfo{};
            poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
            poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
            poolInfo.queueFamilyIndex = graphicsFamily.value();

            if (vkCreateCommandPool(device, &poolInfo, nullptr, &commandPool) != VK_SUCCESS)
                return Err::Failed;

            return Err::OK;
        }

        Err createImage(
            u32 width, u32 height, 
            u32 mipLevels, VkSampleCountFlagBits sampleCount,
            VkFormat format, VkImageTiling tiling, 
            VkImageUsageFlags usage, VkMemoryPropertyFlags properties, 
            VkImage& image, VkDeviceMemory& imageMemory
        ) {
            VkImageCreateInfo imageInfo{};
            imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
            imageInfo.imageType = VK_IMAGE_TYPE_2D;
            imageInfo.extent.width = width;
            imageInfo.extent.height = height;
            imageInfo.extent.depth = 1;
            imageInfo.mipLevels = mipLevels;
            imageInfo.arrayLayers = 1;
            imageInfo.format = format;
            imageInfo.tiling = tiling;
            imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            imageInfo.usage = usage;
            imageInfo.samples = sampleCount;
            imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

            if (vkCreateImage(device, &imageInfo, nullptr, &image) != VK_SUCCESS)
                return Err::Failed;

            VkMemoryRequirements memRequirements{};
            vkGetImageMemoryRequirements(device, image, &memRequirements);

            VkMemoryAllocateInfo allocInfo{};
            allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
            allocInfo.allocationSize = memRequirements.size;
            if (findMemoryType(memRequirements.memoryTypeBits, properties, allocInfo.memoryTypeIndex) != Err::OK)
                return Err::Failed;

            if (vkAllocateMemory(device, &allocInfo, nullptr, &imageMemory) != VK_SUCCESS) {
                throw std::runtime_error("failed to allocate image memory!");
            }

            vkBindImageMemory(device, image, imageMemory, 0);
        }

        using StbPixels = std::unique_ptr<u8, decltype(&stbi_image_free)>;

        Err createTextureImage() {
            s32 texWidth = 0, texHeight = 0, texChannels = 0;
            StbPixels pixels{ stbi_load(TexturePath, &texWidth, &texHeight, &texChannels, STBI_rgb_alpha), stbi_image_free };
            const VkDeviceSize imageSize = texWidth * texHeight * 4;
            mipLevels = static_cast<u32>(std::floor(std::log2(std::max(texWidth, texHeight)))) + 1;

            if (!pixels)
                return Err::Failed;

            VkBuffer stagingBuffer{};
            VkDeviceMemory stagingBufferMemory{};
            const VkMemoryPropertyFlags memProps = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

            if (createBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, memProps, stagingBuffer, stagingBufferMemory) != Err::OK)
                return Err::Failed;

            void* data = nullptr;
            vkMapMemory(device, stagingBufferMemory, 0, imageSize, 0, &data);
            std::memcpy(data, pixels.get(), static_cast<std::size_t>(imageSize));
            vkUnmapMemory(device, stagingBufferMemory);

            const VkImageUsageFlags imageUsage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
            if (createImage(texWidth, texHeight, mipLevels, VK_SAMPLE_COUNT_1_BIT, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_TILING_OPTIMAL, imageUsage, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, textureImage, textureImageMemory) != Err::OK)
                return Err::Failed;

            if (transitionImageLayout(textureImage, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, mipLevels) != Err::OK)
                return Err::Failed;

            if (copyBufferToImage(stagingBuffer, textureImage, static_cast<u32>(texWidth), static_cast<u32>(texHeight)) != Err::OK)
                return Err::Failed;

            //if (transitionImageLayout(textureImage, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, mipLevels) != Err::OK)
            //    return Err::Failed;

            vkDestroyBuffer(device, stagingBuffer, nullptr);
            vkFreeMemory(device, stagingBufferMemory, nullptr);

            return generateMipmaps(textureImage, VK_FORMAT_R8G8B8A8_SRGB, texWidth, texHeight, mipLevels);
        }

        Err generateMipmaps(VkImage image, VkFormat imageFormat, s32 texWidth, s32 texHeight, s32 mipLevels) {
            VkFormatProperties formatProperties{};
            vkGetPhysicalDeviceFormatProperties(physicalDevice, imageFormat, &formatProperties);
            if (!(formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT))
                return Err::Failed;

            VkCommandBuffer commandBuffer = beginSingleTimeCommands();

            VkImageMemoryBarrier barrier{};
            barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
            barrier.image = image;
            barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            barrier.subresourceRange.baseArrayLayer = 0;
            barrier.subresourceRange.layerCount = 1;
            barrier.subresourceRange.levelCount = 1;

            s32 mipWidth = texWidth;
            s32 mipHeight = texHeight;

            for (int i = 1; i < mipLevels; ++i) {
                barrier.subresourceRange.baseMipLevel = i - 1;
                barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
                barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
                barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
                barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

                vkCmdPipelineBarrier(commandBuffer,
                    VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
                    0, nullptr,
                    0, nullptr,
                    1, &barrier);

                VkImageBlit blit{};
                blit.srcOffsets[0] = { 0, 0, 0 };
                blit.srcOffsets[1] = { mipWidth, mipHeight, 1 };
                blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                blit.srcSubresource.mipLevel = i - 1;
                blit.srcSubresource.baseArrayLayer = 0;
                blit.srcSubresource.layerCount = 1;
                blit.dstOffsets[0] = { 0, 0, 0 };
                const s32 offsetWidth = mipWidth > 1 ? mipWidth / 2 : 1;
                const s32 offsetHeight = mipHeight > 1 ? mipHeight / 2 : 1;
                const s32 testW = std::min(offsetWidth, 1);
                const s32 testH = std::max(offsetHeight, 1);
                blit.dstOffsets[1] = { mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1 };
                blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                blit.dstSubresource.mipLevel = i;
                blit.dstSubresource.baseArrayLayer = 0;
                blit.dstSubresource.layerCount = 1;

                vkCmdBlitImage(commandBuffer,
                    image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                    image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                    1, &blit,
                    VK_FILTER_LINEAR);

                barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
                barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
                barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
                barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

                vkCmdPipelineBarrier(commandBuffer,
                    VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
                    0, nullptr,
                    0, nullptr,
                    1, &barrier);

                mipWidth = mipWidth > 1 ? mipWidth / 2 : 1;
                mipHeight = mipHeight > 1 ? mipHeight / 2 : 1;
            }

            barrier.subresourceRange.baseMipLevel = mipLevels - 1;
            barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
            barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

            vkCmdPipelineBarrier(commandBuffer,
                VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
                0, nullptr,
                0, nullptr,
                1, &barrier);

            endSingleTimeCommands(commandBuffer);
            return Err::OK;
        }

        Err createColorResources() {
            const VkFormat colorFormat = swapChainImageFormat;
            const VkImageUsageFlags usageFlags = VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
            
            if (createImage(swapChainExtent.width, swapChainExtent.height, 1, msaaSamples, colorFormat, VK_IMAGE_TILING_OPTIMAL, usageFlags, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, colorImage, colorImageMemory) != Err::OK)
                return Err::Failed;

            if (createImageView(colorImage, colorFormat, VK_IMAGE_ASPECT_COLOR_BIT, colorImageView, 1) != Err::OK)
                return Err::Failed;

            return Err::OK;
        }

        Err createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, VkImageView& imageView, u32 mipLevels) {
            VkImageViewCreateInfo viewInfo{};
            viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            viewInfo.image = image;
            viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
            viewInfo.format = format;
            viewInfo.subresourceRange.aspectMask = aspectFlags;
            viewInfo.subresourceRange.baseMipLevel = 0;
            viewInfo.subresourceRange.levelCount = mipLevels;
            viewInfo.subresourceRange.baseArrayLayer = 0;
            viewInfo.subresourceRange.layerCount = 1;

            if (vkCreateImageView(device, &viewInfo, nullptr, &imageView) != VK_SUCCESS)
                return Err::Failed;

            return Err::OK;
        }

        Err createTextureImageView() {
            return createImageView(textureImage, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_ASPECT_COLOR_BIT, textureImageView, mipLevels);
        }

        Err createCommandBuffers() {
            VkCommandBufferAllocateInfo allocInfo{};
            allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
            allocInfo.commandPool = commandPool;
            allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
            allocInfo.commandBufferCount = static_cast<u32>(commandBuffers.size());

            if (vkAllocateCommandBuffers(device, &allocInfo, commandBuffers.data()) != VK_SUCCESS)
                return Err::Failed;

            return Err::OK;
        }

        Err createSyncObjects() {
            VkSemaphoreCreateInfo semaphoreInfo{};
            semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

            VkFenceCreateInfo fenceInfo{};
            fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
            fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

            for (int i = 0; i < NumConcurrentFrames; ++i) {
                if (vkCreateSemaphore(device, &semaphoreInfo, nullptr, &imageReadySemaphores[i]) != VK_SUCCESS)
                    return Err::Failed;

                if (vkCreateSemaphore(device, &semaphoreInfo, nullptr, &doneRenderingSemaphores[i]) != VK_SUCCESS)
                    return Err::Failed;

                if (vkCreateFence(device, &fenceInfo, nullptr, &inFlightFences[i]) != VK_SUCCESS)
                    return Err::Failed;
            }

            return Err::OK;
        }

        Err findMemoryType(u32 typeFilter, VkMemoryPropertyFlags properties, u32& memType) {
            VkPhysicalDeviceMemoryProperties memProperties;
            vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

            Err err = Err::Failed;

            for (u32 i = 0; i < memProperties.memoryTypeCount; ++i) {
                if (typeFilter & (1 << i) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
                    memType = i;
                    err = Err::OK;
                    break;
                }
            }
            
            return err;
        }

        Err createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory) {
            VkBufferCreateInfo bufferInfo{};
            bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
            bufferInfo.size = size;
            bufferInfo.usage = usage;
            bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

            if (vkCreateBuffer(device, &bufferInfo, nullptr, &buffer) != VK_SUCCESS)
                return Err::Failed;

            VkMemoryRequirements memRequirements{};
            vkGetBufferMemoryRequirements(device, buffer, &memRequirements);

            VkMemoryAllocateInfo allocInfo{};
            allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
            allocInfo.allocationSize = memRequirements.size;

            if (findMemoryType(memRequirements.memoryTypeBits, properties, allocInfo.memoryTypeIndex) != Err::OK)
                return Err::Failed;

            if (vkAllocateMemory(device, &allocInfo, nullptr, &bufferMemory) != VK_SUCCESS)
                return Err::Failed;

            vkBindBufferMemory(device, buffer, bufferMemory, 0);

            return Err::OK;
        }

        Err copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size) {
            VkCommandBuffer tmpCommandBuffer{ beginSingleTimeCommands() };

            VkBufferCopy copyRegion{};
            copyRegion.srcOffset = 0; // Optional
            copyRegion.dstOffset = 0; // Optional
            copyRegion.size = size;
            vkCmdCopyBuffer(tmpCommandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

            endSingleTimeCommands(tmpCommandBuffer);

            return Err::OK;
        }

        Err copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height) {
            VkCommandBuffer commandBuffer = beginSingleTimeCommands();

            VkBufferImageCopy region{};
            region.bufferOffset = 0;
            region.bufferRowLength = 0;
            region.bufferImageHeight = 0;

            region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            region.imageSubresource.mipLevel = 0;
            region.imageSubresource.baseArrayLayer = 0;
            region.imageSubresource.layerCount = 1;

            region.imageOffset = { 0, 0, 0 };
            region.imageExtent = { width, height, 1 };

            vkCmdCopyBufferToImage(
                commandBuffer,
                buffer,
                image,
                VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                1,
                &region
            );

            endSingleTimeCommands(commandBuffer);

            return Err::OK;
        }

        Err transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout, u32 mipLevels) {
            VkCommandBuffer commandBuffer = beginSingleTimeCommands();

            VkImageMemoryBarrier barrier{};
            barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
            barrier.oldLayout = oldLayout;
            barrier.newLayout = newLayout;
            barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

            barrier.image = image;
            barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            barrier.subresourceRange.baseMipLevel = 0;
            barrier.subresourceRange.levelCount = mipLevels;
            barrier.subresourceRange.baseArrayLayer = 0;
            barrier.subresourceRange.layerCount = 1;

            barrier.srcAccessMask = 0; // TODO
            barrier.dstAccessMask = 0; // TODO

            VkPipelineStageFlags sourceStage{};
            VkPipelineStageFlags destStage{};

            if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
                barrier.srcAccessMask = 0;
                barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

                sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
                destStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
            }
            else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
                barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
                barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

                sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
                destStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
            }
            else {
                return Err::Failed;
            }

            vkCmdPipelineBarrier(
                commandBuffer,
                sourceStage, destStage,
                0,
                0, nullptr,
                0, nullptr,
                1, &barrier
            );

            endSingleTimeCommands(commandBuffer);
            return Err::OK;
        }

        Err createVertexBuffer() {

            VkDeviceSize bufferSize = sizeof(verts.front()) * verts.size();
            VkBuffer stagingBuffer{};
            VkDeviceMemory stagingBufferMemory{};

            const VkMemoryPropertyFlags stagingBufferProps = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
            if (createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, stagingBufferProps, stagingBuffer, stagingBufferMemory) != Err::OK)
                return Err::Failed;

            void* gpuMem = nullptr;
            vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &gpuMem);
            std::memcpy(gpuMem, verts.data(), static_cast<std::size_t>(bufferSize));
            vkUnmapMemory(device, stagingBufferMemory);

            const VkBufferUsageFlags vertexBufferUsage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
            if (createBuffer(bufferSize, vertexBufferUsage, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertexBuffer, vertexBufferMemory) != Err::OK)
                return Err::Failed;

            if (copyBuffer(stagingBuffer, vertexBuffer, bufferSize) != Err::OK)
                return Err::Failed;

            vkDestroyBuffer(device, stagingBuffer, nullptr);
            vkFreeMemory(device, stagingBufferMemory, nullptr);

            return Err::OK;
        }

        Err createIndexBuffer() {
            VkDeviceSize bufferSize = sizeof(indices.front()) * indices.size();

            VkBuffer stagingBuffer{};
            VkDeviceMemory stagingBufferMemory{};
            const VkMemoryPropertyFlags stagingBuffersProps = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
            if (createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, stagingBuffersProps, stagingBuffer, stagingBufferMemory) != Err::OK)
                return Err::Failed;

            void* gpuMem = nullptr;
            vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &gpuMem);
            std::memcpy(gpuMem, indices.data(), static_cast<std::size_t>(bufferSize));
            vkUnmapMemory(device, stagingBufferMemory);

            const VkBufferUsageFlags indexBufferUsage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
            if (createBuffer(bufferSize, indexBufferUsage, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, indexBuffer, indexBufferMemory) != Err::OK)
                return Err::Failed;

            if (copyBuffer(stagingBuffer, indexBuffer, bufferSize) != Err::OK)
                return Err::Failed;

            vkDestroyBuffer(device, stagingBuffer, nullptr);
            vkFreeMemory(device, stagingBufferMemory, nullptr);
        }

        Err createUniformBuffers() {
            Err err = Err::OK;
            const VkDeviceSize bufferSize = sizeof(UniformBufferObject);
            const VkMemoryPropertyFlags memProps = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

            for (std::size_t i = 0; i < NumConcurrentFrames && err == Err::OK; i++) {
                err = createBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, memProps, uniformBuffers[i], uniformBuffersMemory[i]);
                if (err == Err::OK) {
                    vkMapMemory(device, uniformBuffersMemory[i], 0, bufferSize, 0, &mappedUniformBuffers[i]);
                }
            }

            return err;
        }

        Err createDescriptorPool() {
            std::array<VkDescriptorPoolSize, 2> poolSizes{};
            poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            poolSizes[0].descriptorCount = static_cast<u32>(NumConcurrentFrames);
            poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            poolSizes[1].descriptorCount = static_cast<u32>(NumConcurrentFrames);

            VkDescriptorPoolCreateInfo poolInfo{};
            poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
            poolInfo.poolSizeCount = static_cast<u32>(poolSizes.size());
            poolInfo.pPoolSizes = poolSizes.data();
            poolInfo.maxSets = static_cast<u32>(NumConcurrentFrames);

            if (vkCreateDescriptorPool(device, &poolInfo, nullptr, &descriptorPool) != VK_SUCCESS)
                return Err::Failed;

            return Err::OK;
        }

        Err createDescriptorSets() {
            std::vector<VkDescriptorSetLayout> layouts(NumConcurrentFrames, descriptorSetLayout);
            VkDescriptorSetAllocateInfo allocInfo{};
            allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
            allocInfo.descriptorPool = descriptorPool;
            allocInfo.descriptorSetCount = static_cast<u32>(NumConcurrentFrames);
            allocInfo.pSetLayouts = layouts.data();

            descriptorSets.resize(NumConcurrentFrames);
            if (vkAllocateDescriptorSets(device, &allocInfo, descriptorSets.data()) != VK_SUCCESS)
                return Err::Failed;

            for (std::size_t i = 0; i < NumConcurrentFrames; i++) {
                VkDescriptorBufferInfo bufferInfo{};
                bufferInfo.buffer = uniformBuffers[i];
                bufferInfo.offset = 0;
                bufferInfo.range = sizeof(UniformBufferObject);

                VkDescriptorImageInfo imageInfo{};
                imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
                imageInfo.imageView = textureImageView;
                imageInfo.sampler = textureSampler;

                std::array<VkWriteDescriptorSet, 2> descriptorWrites{};
                descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
                descriptorWrites[0].dstSet = descriptorSets[i];
                descriptorWrites[0].dstBinding = 0;
                descriptorWrites[0].dstArrayElement = 0;
                descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                descriptorWrites[0].descriptorCount = 1;
                descriptorWrites[0].pBufferInfo = &bufferInfo;
                descriptorWrites[0].pImageInfo = nullptr; // Optional
                descriptorWrites[0].pTexelBufferView = nullptr; // Optional

                descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
                descriptorWrites[1].dstSet = descriptorSets[i];
                descriptorWrites[1].dstBinding = 1;
                descriptorWrites[1].dstArrayElement = 0;
                descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
                descriptorWrites[1].descriptorCount = 1;
                descriptorWrites[1].pImageInfo = &imageInfo;

                vkUpdateDescriptorSets(device, static_cast<u32>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
            }

            return Err::OK;
        }

        std::optional<VkFormat> findSupportedImageFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features) {
            for (const VkFormat format : candidates) {
                VkFormatProperties props{};
                vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &props);

                if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
                    return format;
                }
                else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
                    return format;
                }
            }

            return std::nullopt;
        }

        std::optional<VkFormat> findDepthFormat() {
            return findSupportedImageFormat(
                { VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
                VK_IMAGE_TILING_OPTIMAL,
                VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
            );
        }

        bool hasStencilComponent(VkFormat format) const noexcept {
            return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
        }

        Err createDepthResources() {
            std::optional<VkFormat> depthFormat = findDepthFormat();
            if (!depthFormat.has_value())
                return Err::Failed;

            if (createImage(swapChainExtent.width, swapChainExtent.height, 1, msaaSamples, *depthFormat, VK_IMAGE_TILING_OPTIMAL,
                VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, depthImage, depthImageMemory) != Err::OK)
                return Err::Failed;

            if (createImageView(depthImage, *depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT, depthImageView, 1) != Err::OK)
                return Err::Failed;

            return Err::OK;
        }

        Err initVulkan() {
            if (createInstance() != Err::OK)
                return Err::Failed;

            if (createSurface() != Err::OK)
                return Err::Failed;

            if (pickPhysicalDevice() != Err::OK)
                return Err::Failed;

            if (createLogicalDevice() != Err::OK)
                return Err::Failed;

            if (createSwapChain() != Err::OK)
                return Err::Failed;

            if (createImageViews() != Err::OK)
                return Err::Failed;

            if (createRenderPass() != Err::OK)
                return Err::Failed;

            if (createDescriptorSetLayout() != Err::OK)
                return Err::Failed;

            if (createPipeline() != Err::OK)
                return Err::Failed;

            if (createCommandPool() != Err::OK)
                return Err::Failed;

            if (createColorResources() != Err::OK)
                return Err::Failed;

            if (createDepthResources() != Err::OK)
                return Err::Failed;

            if (createFramebuffers() != Err::OK)
                return Err::Failed;

            if (createTextureImage() != Err::OK)
                return Err::Failed;

            if (createTextureImageView() != Err::OK)
                return Err::Failed;

            if (createTextureSampler() != Err::OK)
                return Err::Failed;

            if (loadModel() != Err::OK) {
                return Err::Failed;
            }

            if (createVertexBuffer() != Err::OK)
                return Err::Failed;

            if (createIndexBuffer() != Err::OK)
                return Err::Failed;

            if (createUniformBuffers() != Err::OK)
                return Err::Failed;

            if (createDescriptorPool() != Err::OK)
                return Err::Failed;

            if (createDescriptorSets() != Err::OK)
                return Err::Failed;

            if (createCommandBuffers() != Err::OK)
                return Err::Failed;

            if (createSyncObjects() != Err::OK)
                return Err::Failed;

            return Err::OK;
        }

        Err initWindow() {
            glfwInit();

            glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
            //glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

            window = glfwCreateWindow(WindowWidth, WindowHeight, "HelloTri", nullptr, nullptr);
            if (window) {
                glfwSetWindowUserPointer(window, this);
                glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);
            }

            return window ? Err::OK : Err::Failed;
        }

        VkCommandBuffer beginSingleTimeCommands() {
            VkCommandBufferAllocateInfo allocInfo{};
            allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
            allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
            allocInfo.commandPool = commandPool;
            allocInfo.commandBufferCount = 1;

            VkCommandBuffer commandBuffer;
            vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);

            VkCommandBufferBeginInfo beginInfo{};
            beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

            vkBeginCommandBuffer(commandBuffer, &beginInfo);

            return commandBuffer;
        }

        void endSingleTimeCommands(VkCommandBuffer commandBuffer) {
            vkEndCommandBuffer(commandBuffer);

            VkSubmitInfo submitInfo{};
            submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
            submitInfo.commandBufferCount = 1;
            submitInfo.pCommandBuffers = &commandBuffer;

            vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
            vkQueueWaitIdle(graphicsQueue);

            vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
        }

        Err recordCommands(VkCommandBuffer commandBuffer, u32 imageIndex) {
            VkCommandBufferBeginInfo beginInfo{};
            beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            beginInfo.flags = 0; // Optional
            beginInfo.pInheritanceInfo = nullptr; // Optional

            if (vkBeginCommandBuffer(commandBuffer, &beginInfo) != VK_SUCCESS)
                return Err::Failed;

            VkRenderPassBeginInfo renderPassInfo{};
            renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            renderPassInfo.renderPass = renderPass;
            renderPassInfo.framebuffer = framebuffers[imageIndex];

            renderPassInfo.renderArea.offset = { 0, 0 };
            renderPassInfo.renderArea.extent = swapChainExtent;

            std::array<VkClearValue, 2> clearColors{};
            clearColors[0].color = { { 0.0f, 0.0f, 0.0f, 1.0f } };
            clearColors[1].depthStencil = { 1.0f, 0 };

            renderPassInfo.clearValueCount = static_cast<u32>(clearColors.size());
            renderPassInfo.pClearValues = clearColors.data();

            vkCmdBeginRenderPass(commandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

            vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
            
            VkDeviceSize offsets[] = { 0 };
            vkCmdBindVertexBuffers(commandBuffer, 0, 1, &vertexBuffer, offsets);
            vkCmdBindIndexBuffer(commandBuffer, indexBuffer, 0, VK_INDEX_TYPE_UINT32);

            VkViewport viewport{};
            viewport.x = 0.0f;
            viewport.y = 0.0f;
            viewport.width = static_cast<f32>(swapChainExtent.width);
            viewport.height = static_cast<f32>(swapChainExtent.height);
            viewport.minDepth = 0.0f;
            viewport.maxDepth = 1.0f;
            vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

            VkRect2D scissor{};
            scissor.offset = { 0, 0 };
            scissor.extent = swapChainExtent;
            vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

            //vkCmdDraw(commandBuffer, static_cast<u32>(quadVertices.size()), 1, 0, 0);
            vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descriptorSets[currentFrame], 0, nullptr);
            vkCmdDrawIndexed(commandBuffer, static_cast<u32>(indices.size()), 1, 0, 0, 0);

            vkCmdEndRenderPass(commandBuffer);
            if (vkEndCommandBuffer(commandBuffer) != VK_SUCCESS)
                return Err::Failed;

            return Err::OK;
        }

        Err updateUniformBuffer(u32 frameIndex) {
            static const auto start = Clock::now();
            const auto now = Clock::now();
    
            const f32 timeElapsed = std::chrono::duration_cast<Seconds>(now - start).count();
            UniformBufferObject ubo{};
            ubo.model = glm::rotate(glm::identity<glm::mat4>(), timeElapsed * glm::radians(90.0f), glm::vec3{ 0.0f, 0.0f, 1.0f });
            //ubo.model = glm::identity<glm::mat4>();
            ubo.view = glm::lookAt(glm::vec3{ 2.0f, 2.0f, 2.0f }, glm::vec3{ 0.0f, 0.0f, 0.0f }, glm::vec3{ 0.0f, 0.0f, 1.0f });
            //ubo.view = glm::lookAt(glm::vec3{ 0.0f, 0.0f, 2.0f }, glm::vec3{ 0.0f, 0.0f, 1.0f }, glm::vec3{ 0.0f, 1.0f, 0.0f });
            const f32 aspectRatio = swapChainExtent.width / static_cast<f32>(swapChainExtent.height);
            ubo.proj = glm::perspective(glm::radians(45.0f), aspectRatio, 0.1f, 10.0f);
            ubo.proj[1][1] *= -1.0f;

            std::memcpy(mappedUniformBuffers[frameIndex], &ubo, sizeof(ubo));

            return Err::OK;
        }

        VkSampleCountFlagBits getMaxUsableSampleCount() {
            VkPhysicalDeviceProperties deviceProps{};
            vkGetPhysicalDeviceProperties(physicalDevice, &deviceProps);
            const VkSampleCountFlags count = deviceProps.limits.framebufferColorSampleCounts & deviceProps.limits.framebufferDepthSampleCounts;

            static constexpr std::array<VkSampleCountFlagBits, 6> VkSampleCounts{
                VK_SAMPLE_COUNT_64_BIT,
                VK_SAMPLE_COUNT_32_BIT,
                VK_SAMPLE_COUNT_16_BIT,
                VK_SAMPLE_COUNT_8_BIT,
                VK_SAMPLE_COUNT_4_BIT,
                VK_SAMPLE_COUNT_2_BIT
            };

            VkSampleCountFlagBits sampleCount = VK_SAMPLE_COUNT_1_BIT;
            const auto itCount = std::find_if(VkSampleCounts.begin(), VkSampleCounts.end(), [=](const VkSampleCountFlags numSamples) {
                return numSamples & count;
            });

            if (itCount != VkSampleCounts.end())
                sampleCount = *itCount;

            return sampleCount;
        }

        Err loadModel() {
            tinyobj::attrib_t attrib{};
            std::vector<tinyobj::shape_t> shapes{};
            std::vector<tinyobj::material_t> materials{};
            std::string warn{}, err{};

            if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, ModelPath))
                return Err::Failed;

            std::unordered_map<Vertex, u32> uniqueVerts{};

            for (const auto& shape : shapes) {
                for (const auto& index : shape.mesh.indices) {
                    Vertex vertex{};

                    vertex.position = {
                        attrib.vertices[3 * index.vertex_index + 0],
                        attrib.vertices[3 * index.vertex_index + 1],
                        attrib.vertices[3 * index.vertex_index + 2]
                    };

                    vertex.uv = {
                        attrib.texcoords[2 * index.texcoord_index + 0],
                        1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
                    };

                    vertex.color = { 1.0f, 1.0f, 1.0f };

                    if (uniqueVerts.count(vertex) == 0) {
                        uniqueVerts[vertex] = static_cast<u32>(verts.size());
                        verts.push_back(vertex);
                    }
                    indices.push_back(uniqueVerts[vertex]);
                }
            }

            return Err::OK;
        }

        Err drawScene() {
            vkWaitForFences(device, 1, &inFlightFences[currentFrame], VK_TRUE, std::numeric_limits<u64>::max());

            u32 imageIndex = 0;
            VkResult result = vkAcquireNextImageKHR(device, swapChain, UINT64_MAX, imageReadySemaphores[currentFrame], VK_NULL_HANDLE, &imageIndex);

            if (result == VK_ERROR_OUT_OF_DATE_KHR) {
                if (recreateSwapChain() != Err::OK) {
                    return Err::Failed;
                }
            }
            else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
                return Err::Failed;
            }

            if (updateUniformBuffer(currentFrame) != Err::OK)
                return Err::Failed;

            vkResetFences(device, 1, &inFlightFences[currentFrame]);
            vkResetCommandBuffer(commandBuffers[currentFrame], 0);
            
            if (recordCommands(commandBuffers[currentFrame], imageIndex) != Err::OK)
                return Err::Failed;

            VkSubmitInfo submitInfo{};
            submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

            VkSemaphore waitSemaphores[] = { imageReadySemaphores[currentFrame] };
            VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
            submitInfo.waitSemaphoreCount = 1;
            submitInfo.pWaitSemaphores = waitSemaphores;
            submitInfo.pWaitDstStageMask = waitStages;

            submitInfo.commandBufferCount = 1;
            submitInfo.pCommandBuffers = &commandBuffers[currentFrame];

            VkSemaphore signalSemaphores[] = { doneRenderingSemaphores[currentFrame] };
            submitInfo.signalSemaphoreCount = 1;
            submitInfo.pSignalSemaphores = signalSemaphores;

            if (vkQueueSubmit(graphicsQueue, 1, &submitInfo, inFlightFences[currentFrame]) != VK_SUCCESS)
                return Err::Failed;

            VkPresentInfoKHR presentInfo{};
            presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

            presentInfo.waitSemaphoreCount = 1;
            presentInfo.pWaitSemaphores = signalSemaphores;

            VkSwapchainKHR swapChains[] = { swapChain };
            presentInfo.swapchainCount = 1;
            presentInfo.pSwapchains = swapChains;
            presentInfo.pImageIndices = &imageIndex;
            presentInfo.pResults = nullptr; // Optional

            result = vkQueuePresentKHR(presentQueue, &presentInfo);
            currentFrame = (currentFrame + 1) % NumConcurrentFrames;

            if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || resized) {
                resized = false;
                if (recreateSwapChain() != Err::OK) {
                    return Err::Failed;
                }
            }

            return Err::OK;
        }

        Err mainLoop() {
            while (!glfwWindowShouldClose(window)) {
                glfwPollEvents();
                drawScene();
            }

            vkDeviceWaitIdle(device);

            return Err::OK;
        }

        void cleanupSwapChain() {
            for (auto framebuffer : framebuffers) {
                vkDestroyFramebuffer(device, framebuffer, nullptr);
            }

            for (auto imageView : swapChainImageViews) {
                vkDestroyImageView(device, imageView, nullptr);
            }

            vkDestroySwapchainKHR(device, swapChain, nullptr);
        }

        void cleanup() {
            cleanupSwapChain();

            vkDestroyImageView(device, colorImageView, nullptr);
            vkDestroyImage(device, colorImage, nullptr);
            vkFreeMemory(device, colorImageMemory, nullptr);

            vkDestroySampler(device, textureSampler, nullptr);
            vkDestroyImageView(device, textureImageView, nullptr);
            vkDestroyImage(device, textureImage, nullptr);
            vkFreeMemory(device, textureImageMemory, nullptr);

            for (int i = 0; i < NumConcurrentFrames; ++i) {
                vkDestroyBuffer(device, uniformBuffers[i], nullptr);
                vkFreeMemory(device, uniformBuffersMemory[i], nullptr);
            }

            vkDestroyDescriptorPool(device, descriptorPool, nullptr);
            vkDestroyDescriptorSetLayout(device, descriptorSetLayout, nullptr);

            vkDestroyBuffer(device, indexBuffer, nullptr);
            vkFreeMemory(device, indexBufferMemory, nullptr);

            vkDestroyBuffer(device, vertexBuffer, nullptr);
            vkFreeMemory(device, vertexBufferMemory, nullptr);

            for (int i = 0; i < NumConcurrentFrames; ++i) {
                vkDestroySemaphore(device, imageReadySemaphores[i], nullptr);
                vkDestroySemaphore(device, doneRenderingSemaphores[i], nullptr);
                vkDestroyFence(device, inFlightFences[i], nullptr);
            }

            vkDestroyCommandPool(device, commandPool, nullptr);

            vkDestroyPipeline(device, pipeline, nullptr);
            vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
            vkDestroyRenderPass(device, renderPass, nullptr);
            
            vkDestroyDevice(device, nullptr);
            vkDestroySurfaceKHR(vulkanInstance, surface, nullptr);
            vkDestroyInstance(vulkanInstance, nullptr);
            glfwDestroyWindow(window);
            glfwTerminate();
        }
    };

    void framebufferResizeCallback(GLFWwindow* window, int width, int height) {
        HelloTri* app = reinterpret_cast<HelloTri*>(glfwGetWindowUserPointer(window));
        app->frameBufferResized(true);
    }
}

int main() {
    App::HelloTri app{};
    //Result<std::vector<f32>> result{ Err::OK, std::vector<f32>{1.0f, 1.0f} };

    const Err err = app.run();
    return static_cast<int>(err);
}