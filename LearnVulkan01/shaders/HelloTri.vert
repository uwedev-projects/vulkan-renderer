#version 460
#extension GL_KHR_vulkan_glsl : enable
#extension GL_EXT_debug_printf : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inCol;
layout (location = 2) in vec2 inUv;

layout (binding = 0) uniform UniformBuffer {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

layout (location = 0) out vec3 fragColor;
layout (location = 1) out vec2 uv;

void main() {
    gl_Position =  ubo.proj * ubo.view * ubo.model * vec4(inPos, 1.0);
    fragColor = inCol;
    uv = inUv;
}