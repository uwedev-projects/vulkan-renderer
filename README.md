# Vulkan Renderer

Disclaimer:
This is a privat project which I am working on in my spare time. The project is only
supported on Windows and uses CMake as it's build tool. It is is basically my
implementation of the vulkan tutorial. Check it out at: https://vulkan-tutorial.com/

![Alt text](/LearnVulkan01/data/Screenshot.jpg?raw=true "Vulkan Model Renderer")

## Minimum Prerequisites ##

To build the project you'll need:

*  Microsoft Visual Studio [Build Tools] 2022
*  CMake Version 3.0.12
*  Git for Windows

With git and CMake installed all third party dependencies will be pulled during the CMake
configure stage.